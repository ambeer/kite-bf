﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityControl : MonoBehaviour {

    // Use this for initialization

    public float gravityX;
    public float gravityY;
    public float gravityZ;

    void Start () {
        Physics.gravity = new Vector3(gravityX, gravityY, gravityZ);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
