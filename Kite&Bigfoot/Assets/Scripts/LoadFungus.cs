﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadFungus : MonoBehaviour {

    public string LevelToLoad;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Application.LoadLevel(LevelToLoad);
        }
    }
}
